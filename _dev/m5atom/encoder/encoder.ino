#include "Unit_Encoder.h"
#include "M5Atom.h"

Unit_Encoder sensor;
long encoder_value_old = 0;


void setup() 
{
  M5.begin(true, false, true);
  sensor.begin();
  // put your setup code here, to run once:

}

void loop() 
{
  // put your main code here, to run repeatedly:
  bool btn_status = !sensor.getButtonStatus();
  long encoder_value = sensor.getEncoderValue();
  long encoder_value_diff = encoder_value - encoder_value_old;

  diff_to_led(encoder_value,btn_status);

  encoder_value_old = encoder_value;
  serial_print_array(encoder_value, encoder_value_diff, btn_status);
  //serial_print_fudi(encoder_value, encoder_value_diff,  btn_status);

  delay(20);

}

void diff_to_led(int encoder_value, bool btn_status)
{
  if (encoder_value_old != encoder_value) {
    if (encoder_value_old > encoder_value) {
      sensor.setLEDColor(1, 0x00FFFF);
    } else {
      sensor.setLEDColor(2, 0xFF0000);
    }
    encoder_value_old = encoder_value;
  } else {
    sensor.setLEDColor(0, 0x111111);
  }
  if(btn_status)
  {
    sensor.setLEDColor(0,0xFFFFFF);
  }
}

void serial_print_array(int encoder_value, int encoder_value_diff, bool btn_status)
{

  Serial.print(encoder_value);
  Serial.print(" ");
  Serial.print(encoder_value_diff);
  Serial.print(" ");
  Serial.println(btn_status);
}

void serial_print_fudi(int encoder_value, int encoder_value_diff, bool btn_status)
{
  Serial.print("/encoder_value ");
  Serial.println(encoder_value);
  Serial.print("/encoder_value_diff ");
  Serial.println(encoder_value_diff);
  Serial.print("/encoder_button_status ");
  Serial.println(btn_status);
}
