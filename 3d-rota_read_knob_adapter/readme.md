
# Objets-monde rotation encoder adapter

## STL
<a href="arduino/m5-clickrotaread-osc/3d-rota_read_knob_adapter/rotaread-adapter-Body.stl">
rotaread-adapter.stl
</a>


## SVG
![rotaread-adapter.svg](./rotaread-adapter.svg)

## Freecad

[rotaread-adapter.FCStd](./rotaread-adapter.FCStd)