unsigned int sendPort = 53001;
byte ID = 1;

bool static_IP=true;
IPAddress static_IP_destination(10,254,0,10);
IPAddress static_IP_device(10,254,0,11);

#include "Unit_Encoder.h"
#include "M5Atom.h"

Unit_Encoder sensor;
long encoder_value_old = 0;
bool btn_status_old = 0;


CRGB color_old=0x000000;


// ethernet
// ethPOE
#include <Ethernet.h>
EthernetUDP udp;
#include <SPI.h>
#define SCK 22
#define MISO 23
#define MOSI 33
#define CS 19
bool ethernet_connected=0;

// WIFI (WIFI MAC add is only used to retrive a )
// Should retrive the mac address of the wifi chip

IPAddress sendIp(127, 0, 0, 1); //changed in setup() 

//Changer les derniers "chiffres de la mac addresse". 
// Mac adresse should be define with a Unique adresse (Key)
// 4 first byte are define here, last 2 by WIFI MAC last digit (Tracability)
byte ethMac[] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xAE, 0xAE }; 


// UUID 
#include <WiFi.h>
// get mac adress from wifi chip
// change first 3 bytes 


//  MICROOSC MESSAGE
#include <MicroOscUdp.h>
// The number 1024 between the < > below  is the maximum number of bytes reserved for incomming messages.
// Outgoing messages are written directly to the output and do not need more reserved bytes.
MicroOscUdp<1024> oscUdp(&udp, sendIp, sendPort);

int timestamp = 0;

// couleur du led
#define WHITE 0xFFFFFF
#define RED 0xFF0000
#define YELLOW 0xFFFF00
#define GREEN 0x00FF00
#define CYAN 0x00FFFF
#define BLUE 0x0000FF
#define PINK 0xFF00FF
#define OFF 0x000000

void setup() 
{
  M5.begin(true, false, true);
  sensor.begin();
  delay(10);
  set_m5_led(RED); 
  retrive_wifi_mac();
  Ethernet.init(CS);
  SPI.begin(SCK, MISO, MOSI, -1);
  // put your setup code here, to run once:
  check_connection();
  reset_timestamp();

}

void loop() 
{
  if(timestamp<0){reset_timestamp();}
  // put your main code here, to run repeatedly:
  bool btn_status = !sensor.getButtonStatus();
  long encoder_value = sensor.getEncoderValue();
  long encoder_value_diff = encoder_value - encoder_value_old;
  long encoder_direction =  constrain(encoder_value_diff, -1, 1);

  diff_to_led(encoder_value,btn_status);
  
  if(value_changed(encoder_value, btn_status))
  { 
    timestamp++;
  serial_print_array(encoder_value, encoder_value_diff, encoder_direction, btn_status,timestamp);
  serial_print_fudi(encoder_direction, btn_status, timestamp);
  check_connection();
  if(ethernet_connected)
    {
      oscUdp.sendMessage("/click_ts", "ii", btn_status, timestamp);
      oscUdp.sendMessage("/dir_ts", "ii", encoder_direction, timestamp); 
    }
  }
  

  delay(20);

}

bool value_changed(long encoder_value,bool btn_status)
{ 
  bool changed =0;
  if(encoder_value!=encoder_value_old)
  {
    changed=1;
    encoder_value_old = encoder_value;
  }
  if(btn_status!=btn_status_old)
  {
    changed=1;
    btn_status_old = btn_status;
  }
  return changed;
}

void diff_to_led(int encoder_value, bool btn_status)
{
  
  if (encoder_value_old != encoder_value) {
    if (encoder_value_old > encoder_value) {
      //sensor.setLEDColor(1, 0x00FFFF);
    } else {
      //sensor.setLEDColor(2, 0xFF0000);
    }
  } else {
    sensor.setLEDColor(0, 0x111111);
  }
  if(btn_status)
  {
    sensor.setLEDColor(0,0xFFFFFF);
  }
}

void check_connection()
{
  auto link = Ethernet.linkStatus();
  switch (link) {
    case Unknown:
       set_m5_led(PINK); 
      break;
    case LinkON:
      connect_ethernet();
      break;
    case LinkOFF:
      ethernet_connected =0;
      set_m5_led(RED); 
      break;
  }
}

void connect_ethernet()
{
  if(!ethernet_connected)
  {
    set_m5_led(YELLOW);
    if(!static_IP)
    { 
      Ethernet.begin(ethMac);
      udp.begin(8888);
      sendIp=Ethernet.localIP(); //get ip
      sendIp[3]=255; //set LST
      oscUdp.setDestination(sendIp,sendPort);
    }else{
      Ethernet.begin(ethMac, static_IP_device);
      udp.begin(8888);
      oscUdp.setDestination(static_IP_destination,sendPort);
      sendIp=Ethernet.localIP(); //get ip
      sendIp[3]=static_IP_destination[3];
    }
    Serial.print("ETH_IP ");
    Serial.println(Ethernet.localIP());
    Serial.print("SENDING_IP ");
    Serial.println(sendIp);
    Serial.print("SENDING_PORT ");
    Serial.println(sendPort);  
    set_m5_led(GREEN); 
    ethernet_connected =1;
  }
    
}

void serial_print_array(int encoder_value, int encoder_value_diff, int encoder_direction, bool btn_status, int timestamp)
{
  Serial.print(encoder_value);
  Serial.print(" ");
  Serial.print(encoder_value_diff);
  Serial.print(" ");
  Serial.print(encoder_direction);
  Serial.print(" ");
  Serial.print(btn_status);
  Serial.print(" ");
  Serial.println(timestamp);
}

void serial_print_fudi(int encoder_direction, bool btn_status, int timestamp)
{
  Serial.print("/dir_ts ");
  Serial.print(encoder_direction);
  Serial.print(" ");
  Serial.println(timestamp);
  
  Serial.print("/click_ts ");
  Serial.print(btn_status);
  Serial.print(" ");
  Serial.println(timestamp);
}

void set_m5_led(CRGB color)
{
  if(color!=color_old)
  { 
    M5.dis.drawpix(0, color); 
    color_old=color;
  } 
}

void reset_timestamp()
{
  timestamp=0;
  Serial.println("/reset_ts 1");
    if(ethernet_connected)
    {
      oscUdp.sendMessage("/reset_ts", "i", 1);
    }
}

void retrive_wifi_mac()
{
  WiFi.begin();
  uint8_t mac[6];
  esp_efuse_mac_get_default(mac);

//Print WIFI 
  // Serial.print("WIFI_MAC_Address ");
  // for (int i = 0; i < 6; i++) {
  //   Serial.printf("%02X", mac[i]);
  //   if (i < 5) {
  //     Serial.print(":");
  //   }
  // }
  // Serial.println();


  // Change ETH MAC to match 2 last bite of WIFI MAC
  ethMac[4]=mac[4];
  ethMac[5]=mac[5];

  Serial.print("ETH_MAC_Address ");
  for (int i = 0; i < 6; i++) {
    Serial.printf("%02X", ethMac[i]);
    if (i < 5) {
      Serial.print(":");
    }
  }
  Serial.println();
  int UUID = ((int)ethMac[4] << 8) | ethMac[5];
  Serial.print("UUID ");
  Serial.println(UUID);
  
}

